from flask import Flask, render_template, request

from processing import predict, load_models

app = Flask(__name__)


@app.route('/', methods=['get', 'post'])  # 127.0.0.1:5000 + '/' = 127.0.0.1:5000/
def main():
    model, scaler_x, scaler_y = load_models()
    message = "Ничего не введено"
    if request.method == "POST":
        area = request.form.get("area")

        try:
            area = float(area)
            cost = predict(scaler_x.transform([[area]]), model)
            cost = scaler_y.inverse_transform([cost])
            message = f"Стоимость квартиры площадью {area} равна {cost[0][0]} руб."
        except:
            message = f"Вы ввели некорректное значение площади: {area}"

    return render_template("index.html", message=message)


app.run()
